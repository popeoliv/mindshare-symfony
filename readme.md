# How to create a symfony project

1. Clone this repo. `git clone git@gitlab.msu.edu:popeoliv/mindshare-symfony`
2. In the repo, run:
    1. `docker compose pull` to get the latest images for mailhog and mariadb
    2. `docker compose build` to build the the apache/php image
    3. `docker compose up -d` to start the containers
    4. `docker compose down` to stop the containers
    5. `docker compose exec <service_name> bash` to run a shell in the specified container
3. Running `docker compose exec web bash`, we can create a new composer project: `composer create-project symfony/skeleton:"6.1.*" .`
4. Then, run `composer require webapp` to install the web framework.
